### Overview
Debian Package for Atlassian Stash.

This package will install `mysql` as database on localhost,
`nginx` as HTTPS frontend, `haproxy` as SSH frontend (not started by default).
Recommends `postfix` as mailer.

### Step 1. Clone repo to your own copy
```
mkdir atlassian-stash-deb
cd atlassian-stash-deb
git init
git remote add upstream https://bitbucket.org/lelik/atlassian-stash-deb.git
git fetch upstream
git merge upstream/master
```
Then add your Organization repository as `origin`:
```
git remote add origin git@git.acme.com:/namespace/atlassian-stash-deb.git
```
And push changes into it:
```
git push -u origin master
```

### Step 2. Creating SSL keys for Your Stash web site
In root of Git repo:
```
mkdir ssl-certs
cd ssl-certs
openssl req -new -out request.csr
```
Enter password you wish (anyone you like, but remember it), answer on questions
`openssl` asks from you, depends on your Organization rules about issuing
certificates.
```
cat request.csr
```
Perform submit this request for certificate to issue certification system on
your Organization (ask admin for this). In result you must have signed
`certnew.cer` file or something like this.
Then convert private key into nginx key:
```
openssl rsa -in privkey.pem -out ../etc/nginx/ssl/stash.key
```
Enter password you remembered on previous step.
Then download Certificates Chain for you Organization into file `allcas.pem`,
and perform merge for CA certs with your new issued certificate:
```
cat certnew.cer allcas.pem > ../etc/nginx/ssl/stash.crt
cd ..
```
Don't forget to commit these new key and cert:
```
git add etc/nginx/ssl/stash.key
git add etc/nginx/ssl/stash.crt
git commit -m "Update SSL certs"
git push origin master
```
You also can save `ssl-certs` directory in your Git repo, but this is not very
secure:
```
git add ssl-certs
git commit -m "Add artifacts of SSL keys creation"
git push origin master
```

### Step 3. Build package
In Git repo root and on Ubuntu/Debian host:
Update version (do not modify first version part, it used for downloading Stash)
```
dch -i
```
Build
```
debuild
```
Upload to you local Debian distributives repo:
```
debrelease
```
#### Version of Stash and Version of Debian Package
Version of stash is first part of debian package version from `debian/changelog`
first line. For example:
```
atlassian-stash (2.1.2.0) lucid; urgency=low
```
Version of Stash — `2.1.2`
Version of Debian Package — `2.1.2.0`

On package build Stash version is directly put into `postinst` script and it
will uprgade Stash automatically on package install. Script will download Stash
for specified version from Atlassian Downloads site, and install it.

### Setup
Put into ```/etc/apt/sources.list``` (if not exists):
```
deb http://dist.acme.com/packages stable/all/
```
Then:
```
sudo apt-get update && sudo apt-get install atlassian-stash
```
Package will create mysql database on localhost, connect Stash with it.
Please note, after installation package will write message about SSH port
forwarding. It ask you to turn off `22` port for `sshd` and turn on `haproxy`
on port `7999`.

### Startup and shutdown
Start:
```
sudo /etc/init.d/stash start
```
Stop:
```
sudo /etc/init.d/stash stop
```
Restart:
```
sudo /etc/init.d/stash restart
```

### Upgrade Stash. Bad practice
```
sudo /etc/init.d/stash upgrade 2.1.2
```
Where ```2.1.2```, is version published on Atlassian Site. This script will
download tar.gz, unpack it into `/usr/local/stash-2.1.2` and link to
`/usr/local/stash`. Then Stash will be restarted.

Version upgrading on first install will performed automatically.

### Upgrade Stash. Good practice.
Update package via:
```
dch -i
```
Change Stash version to new one.
```
git commit -a -m "Upgrade to Stash 2.1.2"
git push origin master
debuild
debrelease
```
Then just install new package on target hosts.

### SSH port forward from `7999` to `22`
Edit file `/etc/ssh/sshd_config` and change `Port` from `22` to `2222`.
Restart `sshd`:
```
sudo /etc/init.d/ssh restart
```
After this close SSH session and login on host again:
```
ssh <your_stash_host> -p 2222
```
Startup haproxy:
```
sudo /etc/init.d/stash-haproxy start
```
Go into Stash Administration web page and change SSH base URL to
`ssh://<your_stash_host>` without port.

**Warning!**
You just changed standard SSH port to `2222`, always use follow SSH to logon on
host:
```
ssh <your_stash_host> -p 2222
```

### Database

This package is used `mysql` as one of most popular on Ubuntu.

Database `stash` created automatically on `localhost` for user `stash`, with
password `stash` on first install. For security issues you can modify this
package as you wish, and as your Organization is requires. Just for this repo.

