#!/bin/sh
### BEGIN INIT INFO
# Provides:          stash-haproxy
# Required-Start:    $local_fs $network $remote_fs
# Required-Stop:     $local_fs $remote_fs
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: fast and reliable load balancing reverse proxy
# Description:       This file should be used to start and stop haproxy.
### END INIT INFO

# Author: Arnaud Cornet <acornet@debian.org>
# Author: Alexey Efimov <aefimov-box@ya.ru>

PATH=/sbin:/usr/sbin:/bin:/usr/bin
PIDFILE=/var/run/stash-haproxy.pid
CONFIG=/etc/stash/haproxy.cfg
HAPROXY=/usr/sbin/haproxy
EXTRAOPTS=

test -x $HAPROXY || exit 0
test -f "$CONFIG" || exit 0

if [ -e /etc/default/haproxy ]; then
    . /etc/default/haproxy
fi

[ -f /etc/default/rcS ] && . /etc/default/rcS
. /lib/lsb/init-functions

haproxy_start()
{
    start-stop-daemon --start --pidfile "$PIDFILE" \
        --exec $HAPROXY -- -f "$CONFIG" -D -p "$PIDFILE" \
        $EXTRAOPTS || return 2
    return 0
}

haproxy_stop()
{
    if [ ! -f $PIDFILE ] ; then
        # This is a success according to LSB
        return 0
    fi
    for pid in $(cat $PIDFILE) ; do
        /bin/kill $pid || return 4
    done
    rm -f $PIDFILE
    return 0
}

haproxy_reload()
{
    $HAPROXY -f "$CONFIG" -p $PIDFILE -D $EXTRAOPTS -sf $(cat $PIDFILE) \
        || return 2
    return 0
}

haproxy_status()
{
    if [ ! -f $PIDFILE ] ; then
        # program not running
        return 3
    fi

    for pid in $(cat $PIDFILE) ; do
        if ! ps --no-headers p "$pid" | grep haproxy > /dev/null ; then
            # program running, bogus pidfile
            return 1
        fi
    done

    return 0
}


case "$1" in
start)
    log_daemon_msg "Starting Stash haproxy" "stash-haproxy"
    haproxy_start
    ret=$?
    case "$ret" in
    0)
        log_end_msg 0
        ;;
    1)
        log_end_msg 1
        echo "pid file '$PIDFILE' found, haproxy not started."
        ;;
    2)
        log_end_msg 1
        ;;
    esac
    exit $ret
    ;;
stop)
    log_daemon_msg "Stopping Stash haproxy" "stash-haproxy"
    haproxy_stop
    ret=$?
    case "$ret" in
    0|1)
        log_end_msg 0
        ;;
    2)
        log_end_msg 1
        ;;
    esac
    exit $ret
    ;;
reload|force-reload)
    log_daemon_msg "Reloading Stash haproxy" "stash-haproxy"
    haproxy_reload
    case "$?" in
    0|1)
        log_end_msg 0
        ;;
    2)
        log_end_msg 1
        ;;
    esac
    ;;
restart)
    log_daemon_msg "Restarting Stash haproxy" "stash-haproxy"
    haproxy_stop
    haproxy_start
    case "$?" in
    0)
        log_end_msg 0
        ;;
    1)
        log_end_msg 1
        ;;
    2)
        log_end_msg 1
        ;;
    esac
    ;;
status)
    haproxy_status
    ret=$?
    case "$ret" in
    0)
        echo "stash-haproxy is running."
        ;;
    1)
        echo "stash-haproxy dead, but $PIDFILE exists."
        ;;
    *)
        echo "stash-haproxy not running."
        ;;
    esac
    exit $ret
    ;;
*)
    echo "Usage: /etc/init.d/stash-haproxy {start|stop|reload|restart|status}"
    exit 2
    ;;
esac

:
